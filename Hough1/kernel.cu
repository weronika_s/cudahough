﻿
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <chrono>
#include <stdio.h>
#include <string>
#include <iostream>
#include "lodepng.h"

#include <cuda.h>
//#include <device_functions.h>
#include <cuda_runtime_api.h>
#include <device_functions.h>

#define PI 3.13

cudaError_t houghWithCuda(unsigned char* image, unsigned int* output, int width, int height, int acc_width, int acc_height);

__global__ void houghKernel(unsigned char* in, unsigned int* out,int width, int height,int acc_height, int acc_width)
{
	
	//int i = threadIdx.x;
	const unsigned int idx = blockIdx.x*blockDim.x + threadIdx.x;
	
	int col = idx % width;
	int row = (idx) / width;
		
	//if (row*col > height*width)
	//	return;
	//hitted[col*row] = 1;

	double center_x = width / 2;	//po³owa szerokoœci obrazka
	double center_y = height / 2;	//po³owa wysokoœci obrazka

	int value = in[idx];
	if (value == 0) return;

	for (int theta = 0; theta < 360; theta++)
	{
		double radians = theta * PI / 180;
		double r = ((double)col - center_x - 1) * cos(radians) + ((double)row - center_y - 1) * sin(radians);
		int ro = (int)(r + acc_height / 2 + 1);
		//out[ro*acc_width + theta]++;	
		atomicAdd(out+ ro * acc_width + theta, 1);
	}
	
}


int main()
{
	printf("Wprowadz sciezke do pliku:\n");
	std::string input;
	std::getline(std::cin, input);

	printf("Wprowadz sciezke do zapisu (wraz z nazwa pliku):\n");
	std::string output;
	std::getline(std::cin, output);

	//załadowanie pliku z podanej ścieżki 

	std::vector<unsigned char> png;
	std::vector<unsigned char> image; 
	
	unsigned width, height;

	lodepng::load_file(png, input);
	unsigned error = lodepng::decode(image, width, height, png);
	if (error) {
		std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
		return 1;
	}

	/*
		przetworzenie zdekodowanego obrazu, obraz jest w formacie RGBARGBARGBA dla każdego pixela 4 wartości
		After decode image is
			0 1 2 3 4 5 6 7
			R G B A R G B A
	*/

	unsigned char* input_image = new unsigned char[image.size() / 4];
	int where = 0;
	for (int i = 0; i < image.size(); i += 4) {
		
		input_image[where] = image.at(i);
		where++;
	}

	//alokacja tablicy głosów i bitmapy na output
	int acc_height = sqrt(width*width + height * height) + 1;
	int acc_width = 360;
	unsigned int *accumulator = (unsigned int*)calloc(acc_height*acc_width, sizeof(unsigned int));
	int* hitted = (int*)calloc(height*width, sizeof(int));
		
    // funkcja transformaty na karcie graficznej 	
    cudaError_t cudaStatus = houghWithCuda(input_image,accumulator,width,height,acc_width,acc_height);

    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "addWithCuda failed!");
        return 1;
    }

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceReset failed!");
		return 1;
	}
	
	auto start_time = std::chrono::high_resolution_clock::now();
	for (int idx = 0; idx < height*width; idx++) {
		int col = idx % width;
		int row = (idx) / width;

		if (row*col > height*width)
			continue;

		double center_x = width / 2;	//polowa szerokoœci obrazka
		double center_y = height / 2;	//polowa wysokoœci obrazka

		int value = input_image[row*width + col];
		if (value == 0) continue;

		for (int theta = 0; theta < 360; theta++)
		{
			double radians = theta * PI / 180;
			double r = ((double)col - center_x - 1) * cos(radians) + ((double)row - center_y - 1) * sin(radians);
			int ro = (int)(r + acc_height / 2 + 1);
			accumulator[ro*acc_width + theta]++;
		}		
	}

	auto end_time = std::chrono::high_resolution_clock::now();
	auto milliseconds = (end_time - start_time) / std::chrono::milliseconds(1);
	std::cout << "Transformacja Hough'a zajela " << milliseconds << " milisekund.\n";
	//normalizacja tablicy wyników
	
	int max = 0;
	for (int i = 0; i < acc_width*acc_height; i++) {
		if (accumulator[i] > max)
			max = accumulator[i];
	}
	
	for (int ro = 0; ro < acc_height*acc_width; ro++) {
			accumulator[ro] = (int)((accumulator[ro] * 255.0 )/ max);
	}
	
	/*
	przygotowanie obrazu wynikowego do formatu RGBARGBARGBA 
	R=G=A=accumulator[i] a Alpha=255
	*/
	std::vector<unsigned char> out_image;
	where = 0;
	for (int i = 0; i < (acc_width*acc_height)*4; i+=4) {
		out_image.push_back(accumulator[where]);
		out_image.push_back(accumulator[where]);
		out_image.push_back(accumulator[where++]);
		out_image.push_back(255);
	}	

	// zapis obrazu
	error = lodepng::encode(output, out_image, acc_width, acc_height);

	if (error) std::cout << "encoder error " << error << ": " << lodepng_error_text(error) << std::endl;
	   	
    return 0;
}

cudaError_t houghWithCuda(unsigned char* image, unsigned int* output, int width, int height, int acc_width, int acc_height) {
	unsigned char *dev_input = 0;
	unsigned int *dev_output = 0;
	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		goto Error;
	}

	// Allocate GPU buffers for three vectors (two input, one output)    .
	cudaStatus = cudaMalloc((void**)&dev_input, width*height * sizeof(unsigned char));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&dev_output, acc_width*acc_height * sizeof(unsigned int));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	// Copy input vectors from host memory to GPU buffers.
	cudaStatus = cudaMemcpy(dev_input, image, width*height * sizeof(unsigned char), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

	struct cudaDeviceProp properties;
	cudaGetDeviceProperties(&properties, 0);
	std::cout << "using " << properties.multiProcessorCount << " multiprocessors" << std::endl;
	std::cout << "max threads per processor: " << properties.maxThreadsPerMultiProcessor << std::endl;

	// Launch a kernel on the GPU with one thread for each element.
	//dim3 gridDims((unsigned int)ceil((double)(width*height/ blockDims.x)), 1, 1);
	dim3 blockDims(1024, 1, 1);
	int size = (height*width) / 1024 + 1;
	dim3 gridDims((unsigned int)size/2,  2, 1);
	auto start_time = std::chrono::high_resolution_clock::now();
	
	int i = 0;
	//houghKernel<<<gridDims,blockDims>>>(dev_input, dev_output,width,height,acc_height,acc_width,hitted,&i);
	houghKernel<<<(height*width) / 1024 + 1, 1024>>>(dev_input, dev_output,width,height,acc_height,acc_width);
	
	
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
		goto Error;
	}

	auto end_time = std::chrono::high_resolution_clock::now();
	auto milliseconds = (end_time - start_time) / std::chrono::milliseconds(1);
	std::cout << "Transformacja Hough'a na karcie CUDA zajela " << milliseconds << " milisekund.\n";
	
	// Copy output vector from GPU buffer to host memory.
	cudaStatus = cudaMemcpy(output, dev_output, acc_width*acc_height * sizeof(unsigned int), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}
	
Error:
	cudaFree(dev_input);
	cudaFree(dev_output);

	return cudaStatus;

}

